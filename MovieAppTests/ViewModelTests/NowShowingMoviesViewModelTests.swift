//
//  NowShowingMoviesViewModelTests.swift
//  MovieAppTests
//
//  Created by Waheed Malik on 01/01/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import XCTest
@testable import MovieApp

extension NowShowingMoviesViewModelTests {
    class MockMovieManager: MovieManager {
        func loadRelatedMovies(ofMovie movie: Movie, completionHandler: @escaping (Result<[Movie]>) -> Void) {
        }
        
        func loadDetail(ofMovie movie: Movie, completionHandler: @escaping (Result<Movie>) -> Void) {
        }
        
        var loadNowShowingMoviesCalled = false
        var completionHandler: ((Result<[Movie]>) -> Void)?
        func loadNowShowingMovies(completionHandler: @escaping (Result<[Movie]>) -> Void) {
            loadNowShowingMoviesCalled = true
            self.completionHandler = completionHandler
        }
    }
    
    class MockNowShowingMoviesViewModelViewDelegate: NowShowingMoviesViewModelViewDelegate {
        var expectation: XCTestExpectation?
        var nowShowingMoviesViewModelResult: Result<Void>?
        
        func nowShowingMoviesLoaded(withResult result: Result<Void>) {
            guard let asynExpectation = expectation else {
                XCTFail("MockNowShowingMoviesViewModelViewDelegate was not setup correctly. Missing XCTExpectation reference")
                return
            }
            self.nowShowingMoviesViewModelResult = result
            asynExpectation.fulfill()
        }
    }
    
    class MockNowShowingMoviesViewModelCoordinatorDelegate: NowShowingMoviesViewModelCoordinatorDelegate {
        func didSelect(movie: Movie) {
        }
    }
}

class NowShowingMoviesViewModelTests: XCTestCase {
    
    var sut: NowShowingMoviesViewModelImpl!
    var mockMovieManager: MockMovieManager!
    var mockViewDelegate: MockNowShowingMoviesViewModelViewDelegate!
    var mockCoordinatorDelegate: MockNowShowingMoviesViewModelCoordinatorDelegate!
    
    override func setUp() {
        super.setUp()
        mockMovieManager = MockMovieManager()
        mockViewDelegate = MockNowShowingMoviesViewModelViewDelegate()
        mockCoordinatorDelegate = MockNowShowingMoviesViewModelCoordinatorDelegate()
        sut = NowShowingMoviesViewModelImpl(movieManager: mockMovieManager, viewDelegate: mockViewDelegate, coordinatorDelegate: mockCoordinatorDelegate)
    }
    
    func testConformanceToNowShowingMoviesViewModelProtocol() {
        // Assert
        XCTAssertTrue((sut as AnyObject) is NowShowingMoviesViewModel, "NowShowingMoviesViewModelImpl should conforms to NowShowingMoviesViewModel protocol")
    }
    
    func testGetNowShowingMovies_shouldAskMovieManagerForListOfMovies() {
        // Act
        sut.getNowShowingMovies()
        // Assert
        XCTAssert(mockMovieManager.loadNowShowingMoviesCalled)
    }
    
    func testGetNowShowingMovies_whenMoviesFound_raisesNowShowingMoviesLoadedDelegateCallbackWithSuccess() {
        // Arrange
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.getNowShowingMovies()
        mockMovieManager.completionHandler?(Result.success([MovieImpl(id: 123)]))
        
        waitForExpectations(timeout: 1.0) {[unowned self] error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            
            switch self.mockViewDelegate.nowShowingMoviesViewModelResult! {
            case .success():
                XCTAssert(true)
            default:
                XCTFail("View delegate should only receive success result when movies found")
            }
        }
    }
    
    func testGetNowShowingMovies_whenNoMoviesFound_raisesNowShowingMoviesLoadedDelegateCallbackWithFailure() {
        // Arrange
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.getNowShowingMovies()
        mockMovieManager.completionHandler?(Result.failure(MovieManagerError.noNowShowingMoviesFoundError))
        
        waitForExpectations(timeout: 1.0) {[unowned self] error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            
            switch self.mockViewDelegate.nowShowingMoviesViewModelResult! {
            case .success():
                XCTFail("View delegate should receive failure with an error when NO movies found")
            case .failure(let error):
                XCTAssertEqual(error as! MovieManagerError, MovieManagerError.noNowShowingMoviesFoundError)
            }
        }
    }
    
    func testNumberOfNowShowingMovies_whenNoMovieFound_returnsZero() {
        // Arrange
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.getNowShowingMovies()
        mockMovieManager.completionHandler?(Result.failure(MovieManagerError.noNowShowingMoviesFoundError))
        
        // Assert
        waitForExpectations(timeout: 1.0) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            XCTAssertEqual(self.sut.numberOfMovies(), 0)
        }
    }
    
    func testNumberOfNowShowingMovies_whenOneMovieFound_returnsOne() {
        // Arrange
        let movies = [MovieImpl(id: 123)]
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.getNowShowingMovies()
        mockMovieManager.completionHandler?(Result.success(movies))
        
        // Assert
        waitForExpectations(timeout: 1.0) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            XCTAssertEqual(self.sut.numberOfMovies(), 1)
        }
    }
    
    func testNumberOfNowShowingMovies_whenFourMovieFound_returnsFour() {
        // Arrange
        let movies = [MovieImpl(id: 123), MovieImpl(id: 223), MovieImpl(id: 145), MovieImpl(id: 253)]
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.getNowShowingMovies()
        mockMovieManager.completionHandler?(Result.success(movies))
        
        // Assert
        waitForExpectations(timeout: 1.0) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            XCTAssertEqual(self.sut.numberOfMovies(), 4)
        }
    }
    
    func testNowShowingMovieAtIndex_returnsTheCorrectMovieViewModelObject() {
        // Arrange
        let movies = [MovieImpl(id: 123, title: "Star Wars"), MovieImpl(id: 223, title: "Jumanje")]
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.getNowShowingMovies()
        mockMovieManager.completionHandler?(Result.success(movies))
        
        // Assert
        waitForExpectations(timeout: 1.0) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            let movie1 = self.sut.movie(at: 0)
            let movie2 = self.sut.movie(at: 1)
            XCTAssertEqual(movie1?.title, "Star Wars")
            XCTAssertEqual(movie2?.title, "Jumanje")
        }
    }
    
    func testNowShowingMovieAtIndex_whenMovieNotFoundOnPassedIndex_returnsNil() {
        // Arrange
        let movies = [MovieImpl(id: 123, title: "Star Wars"), MovieImpl(id: 223, title: "Jumanje")]
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.getNowShowingMovies()
        mockMovieManager.completionHandler?(Result.success(movies))
        
        // Assert
        waitForExpectations(timeout: 1.0) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            let movie = self.sut.movie(at: 2)
            XCTAssertNil(movie)
        }
    }
}
