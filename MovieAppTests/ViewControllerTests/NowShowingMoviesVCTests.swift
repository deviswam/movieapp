//
//  NowShowingMoviesVCTests.swift
//  MovieAppTests
//
//  Created by Waheed Malik on 02/01/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import XCTest
@testable import MovieApp

extension NowShowingMoviesVCTests {
    class MockNowShowingMoviesViewModel: NowShowingMoviesViewModel {
        func selectedMovie(at index: Int) {
        }
        
        var getMoviesCalled = false
        func getNowShowingMovies() {
            getMoviesCalled = true
        }
        
        func numberOfNowShowingMovies() -> Int {
            return 0
        }
        
        func nowShowingMovie(at index: Int) -> MovieViewModel? {
            return nil
        }
    }
    
    class MockUICollectionView : UICollectionView {
        var reloadGotCalled = false
        override func reloadData() {
            reloadGotCalled = true
        }
    }
}

class NowShowingMoviesVCTests: XCTestCase {
    var sut: NowShowingMoviesVC!
    var mockViewModel: MockNowShowingMoviesViewModel!
    
    override func setUp() {
        super.setUp()
        
        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        sut = (storyBoard.instantiateViewController(withIdentifier: "NowShowingMoviesVC") as! NowShowingMoviesVC)
        mockViewModel = MockNowShowingMoviesViewModel()
        sut.viewModel = mockViewModel
    }
    
    func testNowShowingMoviesVCHasAViewModel() {
        // Assert
        XCTAssertNotNil(sut.viewModel)
    }
    
    func testMoviesCollectionView_whenViewIsLoaded_isNotNil() {
        //Act
        _ = sut.view
        // Assert
        XCTAssertNotNil(sut.moviesCollectionView)
    }

    func testMoviesCollectionView_whenViewIsLoaded_hasDataSource() {
        // Act
        sut.moviesCollectionViewDataSource = MoviesCollectionViewDataSource()
        _ = sut.view
        // Assert
        XCTAssert((sut.moviesCollectionViewDataSource as Any) is UICollectionViewDataSource)
    }

    func testViewLoad_shouldAskViewModelForNowShowingMovies() {
        //Act
        _ = sut.view

        //Assert
        XCTAssertTrue(mockViewModel.getMoviesCalled,"view load should call view model for now showing movies")
    }

    func testAfterHavingMovies_ShouldReloadCollectionView() {
        //Arrange
        let mockUICollectionView = MockUICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout())
        sut.moviesCollectionView = mockUICollectionView

        //Act
        sut.nowShowingMoviesLoaded(withResult: .success(()))

        //Assert
        XCTAssertTrue(mockUICollectionView.reloadGotCalled,"CollectionView Reload method should be called")
    }
}

