//
//  MoviesCollectionViewDataSourceTests.swift
//  MovieAppTests
//
//  Created by Waheed Malik on 02/01/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import XCTest
@testable import MovieApp

extension MoviesCollectionViewDataSourceTests {
    class MockNowShowingMoviesViewModel: BaseViewModel {
        func numberOfNowShowingMovies() -> Int {
            return movies!.count
        }
        
        func nowShowingMovie(at index: Int) -> MovieViewModel? {
            return MovieViewModelImpl(movie: movies![index])
        }
    }
    
    class MockCollectionView: UICollectionView {
        var cellGotDequeued = false
        override func dequeueReusableCell(withReuseIdentifier identifier: String, for indexPath: IndexPath) -> UICollectionViewCell {
            cellGotDequeued = true
            return super.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath)
        }
    }
    
    class MockMovieCollectionViewCell : MovieCollectionViewCell {
        var configCellGotCalled = false
        var movie: MovieViewModel?
        override func configCell(with movieViewModel: MovieViewModel) {
            configCellGotCalled = true
            self.movie = movieViewModel
        }
    }
}

class MoviesCollectionViewDataSourceTests: XCTestCase {
    var sut: MoviesCollectionViewDataSource!
    var mockCollectionView: MockCollectionView!
    var mockViewModel: MockNowShowingMoviesViewModel!
    
    override func setUp() {
        super.setUp()
        sut = MoviesCollectionViewDataSource()
        mockViewModel = MockNowShowingMoviesViewModel()
        mockCollectionView = MockCollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout())
        sut.viewModel = mockViewModel
        mockCollectionView.dataSource = sut
    }
    
    
    func testDataSourceHasNowShowingMoviesViewModel() {
        // Assert
        XCTAssertNotNil(sut.viewModel)
        XCTAssertNotNil((sut.viewModel! as Any) is NowShowingMoviesViewModel)
    }
    
    func testNumberOfSections_IsOne() {
        //Arrange
        let noOfSections = sut.numberOfSections(in: mockCollectionView)
        //Assert
        XCTAssertEqual(noOfSections, 1,"CollectionView has only one section")
    }
    
    func testNumberOfItemsInTheSection_WithTwoMovies_ShouldReturnTwo() {
        //Arrange
        let movies = [MovieImpl(id: 123), MovieImpl(id: 423)]
        mockViewModel.movies = movies
        
        //Act
        let noOfItemsInSectionZero = sut.collectionView(mockCollectionView, numberOfItemsInSection: 0)
        
        //Assert
        XCTAssertEqual(noOfItemsInSectionZero, 2, "Number of items in section 0 are 2")
    }
    
    func testCellForItem_ReturnsMovieCollectionViewCell() {
        //Arrange
        let movies = [MovieImpl(id: 123), MovieImpl(id: 423)]
        mockViewModel.movies = movies
        
        mockCollectionView.register(MockMovieCollectionViewCell.self, forCellWithReuseIdentifier: "MovieCell")
        
        //Act
        let cell = sut.collectionView(mockCollectionView, cellForItemAt: IndexPath(item: 0, section: 0))
        
        //Assert
        XCTAssertTrue(cell is MovieCollectionViewCell,"Returned cell is of MovieCollectionViewCell type")
    }
    
    func testCellForItem_DequeuesCell() {
        //Arrange
        let movies = [MovieImpl(id: 123), MovieImpl(id: 423)]
        mockViewModel.movies = movies
        
        mockCollectionView.register(MockMovieCollectionViewCell.self, forCellWithReuseIdentifier: "MovieCell")
        
        //Act
        _ = sut.collectionView(mockCollectionView, cellForItemAt: IndexPath(item: 0, section: 0))
        
        //Assert
        XCTAssertTrue(mockCollectionView.cellGotDequeued,"CellForItem should be calling DequeueCell method")
    }
    
    func testConfigCell_GetsCalledFromCellForItem() {
        //Arrange
        let movies = [MovieImpl(id: 123, title: "Star Wars")]
        mockViewModel.movies = movies

        mockCollectionView.register(MockMovieCollectionViewCell.self, forCellWithReuseIdentifier: "MovieCell")

        //Act
        let cell = sut.collectionView(mockCollectionView, cellForItemAt: IndexPath(item: 0, section: 0)) as! MockMovieCollectionViewCell

        //Assert
        XCTAssertTrue(cell.configCellGotCalled,"CellForItem should be calling ConfigCell method")
        XCTAssertTrue(cell.movie?.title == movies.first?.title, "Movie title should be same")
    }
}
