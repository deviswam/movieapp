//
//  MovieDbAPIClientTests.swift
//  MovieAppTests
//
//  Created by Waheed Malik on 01/01/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import XCTest
@testable import MovieApp

extension MovieDbAPIClientTests {
    class MockURLSession: URLSession {
        typealias Handler = (Data?, URLResponse?, Error?) -> Void
        
        var dataTaskRequestMethodCalled = false
        var urlRequest : URLRequest?
        var completionHandler : Handler?
        var dataTask = MockURLSessionDataTask()
        
        override func dataTask(with urlRequest: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
            dataTaskRequestMethodCalled = true
            self.urlRequest = urlRequest
            self.completionHandler = completionHandler
            return dataTask
        }
    }
    
    class MockURLSessionDataTask : URLSessionDataTask {
        var resumeGotCalled = false
        override func resume() {
            resumeGotCalled = true
        }
    }
}

class MovieDbAPIClientTests: XCTestCase {
    var mockURLSession: MockURLSession!
    var sut: MovieDbAPIClient!
    
    override func setUp() {
        super.setUp()
        mockURLSession = MockURLSession()
        sut = MovieDbAPIClient(urlSession: mockURLSession)
    }
    
    func testConformanceToMovieAPIClient() {
        XCTAssertTrue((sut as AnyObject) is MovieAPIClient)
    }
    
    func testFetchNowShowingMovies_shouldAskURLSessionForFetchingNowShowingMoviesData() {
        // Act
        sut.fetchNowShowingMovies { result in
            
        }
        
        // Assert
        XCTAssert(mockURLSession.dataTaskRequestMethodCalled)
    }
    
    func testFetchNowShowingMovies_withValidJSONOfAMovie_shouldReturnAMovieAndNilError() {
        //Arrange
        let asynExpectation = expectation(description: "AsynFunction")
        
        let movieAPIResponseInString = "{\"results\":[{" +
            "\"id\":181808," +
            "\"title\":\"Star Wars\"," +
            "\"vote_average\":7.3," +
            "\"poster_path\":\"/xGWVjewoXnJhvxKW619cMzppJDQ.jpg\"" +
        "}]}"
        
        let responseData = movieAPIResponseInString.data(using: .utf8)
        var receivedMovie : Movie?
        var receivedError : MovieAPIClientError?
        
        //Act
        sut.fetchNowShowingMovies { (result: Result<[Movie]>) in
            switch result {
            case .success(let movies):
                receivedMovie = movies.first
            case .failure(let error):
                receivedError = error as? MovieAPIClientError
            }
            asynExpectation.fulfill()
        }
        mockURLSession.completionHandler!(responseData, nil, nil)
        
        waitForExpectations(timeout: 1.0) { error in
            //Assert
            XCTAssertEqual(receivedMovie?.id, 181808, "Fetched and expected movie id should be same")
            XCTAssertEqual(receivedMovie?.title, "Star Wars", "Fetched and expected movie title should be same")
            XCTAssertEqual(receivedMovie?.voteAverage, 7.3, "Fetched and expected movie vote should be same")
            XCTAssertEqual(receivedMovie?.posterPath, "/xGWVjewoXnJhvxKW619cMzppJDQ.jpg", "Fetched and expected movie poster path should be same")
            XCTAssertNil(receivedError)
        }
    }
    
    func testFetchNowShowingMovies_withValidJSONOfTwoMovies_shouldReturnTwoMovieObjectsAndNilError() {
        //Arrange
        let asynExpectation = expectation(description: "AsynFunction")

        let movieAPIResponseInString = "{\"results\":[{" +
            "\"id\":181808," +
            "\"title\":\"Star Wars\"," +
            "\"vote_average\":7.3," +
            "\"poster_path\":\"/xGWVjewoXnJhvxKW619cMzppJDQ.jpg\"" +
            "},{" +
            "\"id\":898989," +
            "\"title\":\"Jumanjee\"," +
            "\"vote_average\":5.3," +
            "\"poster_path\":\"/xGWVjewoXnJhvxKW619cMzppJDQ.jpg\"" +
        "}]}"

        let responseData = movieAPIResponseInString.data(using: .utf8)
        var receivedMovies : [Movie]!
        var receivedError : MovieAPIClientError?

        //Act
        sut.fetchNowShowingMovies { (result: Result<[Movie]>) in
            switch result {
            case .success(let movies):
                receivedMovies = movies
            case .failure(let error):
                receivedError = error as? MovieAPIClientError
            }
            asynExpectation.fulfill()
        }
        mockURLSession.completionHandler!(responseData, nil, nil)

        waitForExpectations(timeout: 1.0) { error in
            //Assert
            //Assert
            XCTAssertEqual(receivedMovies[0].id, 181808, "Fetched and expected movie id should be same")
            XCTAssertEqual(receivedMovies[0].title, "Star Wars", "Fetched and expected movie title should be same")
            XCTAssertEqual(receivedMovies[0].voteAverage, 7.3, "Fetched and expected movie vote should be same")
            XCTAssertEqual(receivedMovies[0].posterPath, "/xGWVjewoXnJhvxKW619cMzppJDQ.jpg", "Fetched and expected movie poster path should be same")

            XCTAssertEqual(receivedMovies[1].id, 898989, "Fetched and expected movie id should be same")
            XCTAssertEqual(receivedMovies[1].title, "Jumanjee", "Fetched and expected movie title should be same")
            XCTAssertEqual(receivedMovies[1].voteAverage, 5.3, "Fetched and expected movie vote should be same")
            XCTAssertEqual(receivedMovies[1].posterPath, "/xGWVjewoXnJhvxKW619cMzppJDQ.jpg", "Fetched and expected movie poster path should be same")

            XCTAssertNil(receivedError)
        }
    }
    
    func testFetchNowShowingMovies_withInvalidJSON_shouldReturnDataDecodingErrorAndNilMovies() {
        //Arrange
        let asynExpectation = expectation(description: "AsynFunction")
        
        let movieAPIResponseInString = "{\"RESULT\":[{" +
            "\"id\":181808," +
            "\"title\":\"Star Wars\"," +
            "\"vote_average\":7.3," +
            "\"poster_path\":\"/xGWVjewoXnJhvxKW619cMzppJDQ.jpg\"" +
        "}]}"
        
        let responseData = movieAPIResponseInString.data(using: .utf8)
        var receivedMovies : [Movie]?
        var receivedError : MovieAPIClientError?
        
        //Act
        sut.fetchNowShowingMovies { (result: Result<[Movie]>) in
            switch result {
            case .success(let movies):
                receivedMovies = movies
            case .failure(let error):
                receivedError = error as? MovieAPIClientError
            }
            asynExpectation.fulfill()
        }
        mockURLSession.completionHandler!(responseData, nil, nil)
        
        waitForExpectations(timeout: 1.0) { error in
            //Assert
            XCTAssertTrue(receivedError! == MovieAPIClientError.dataDecodingError)
            XCTAssertNil(receivedMovies)
        }
    }
}
