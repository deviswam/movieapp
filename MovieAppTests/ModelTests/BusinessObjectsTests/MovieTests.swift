//
//  MovieTests.swift
//  MovieAppTests
//
//  Created by Waheed Malik on 01/01/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import XCTest
@testable import MovieApp

class MovieTests: XCTestCase {
    
    func testConformanceToMovieProtocol() {
        // Arrange
        let sut = MovieImpl(id: 123)
        // Assert
        XCTAssert((sut as Any) is Movie)
    }
    
    func testInit_movieShouldHaveATitle() {
        // Arrange
        let movieTitle = "StarWars"
        let sut = MovieImpl(id: 123, title: movieTitle)
        // Assert
        XCTAssertEqual(sut.title, movieTitle)
    }
    
    func testInit_movieShouldHaveAReleaseDate() {
        // Arrange
        let date = Date()
        var sut = MovieImpl(id: 123)
        sut.releaseDate = date
        // Assert
        XCTAssertEqual(sut.releaseDate, date)
    }
    
    func testInit_movieShouldHaveAVoteAverage() {
        // Arrange
        let voteAvg: Float = 7.1
        let sut = MovieImpl(id: 123, voteAverage: voteAvg)
        // Assert
        XCTAssertEqual(sut.voteAverage, voteAvg)
    }
    
    func testInit_movieShouldHaveAnOverview() {
        // Arrange
        let overView = "This is StarWars overview"
        var sut = MovieImpl(id: 123)
        sut.overView = overView
        // Assert
        XCTAssertEqual(sut.overView, overView)
    }
    
    func testInit_movieShouldHaveAPosterPath() {
        // Arrange
        let posterPath = "StarWarsPoster.jpg"
        let sut = MovieImpl(id: 123, posterPath: posterPath)
        // Assert
        XCTAssertEqual(sut.posterPath, posterPath)
    }
    
    func testMoviesForEquality_withSameId_areSameMovies() {
        //Arrange
        let movie1 = MovieImpl(id: 123, title: "Star Wars")
        let movie2 = MovieImpl(id: 123)
        
        //Assert
        XCTAssertTrue(movie1 == movie2, "Both are same movie")
    }
    
    func testInit_withInvalidMovieJSONData_shouldFailMovieDecoding() {
        //Arrange
        let sut = try? JSONDecoder().decode(MovieImpl.self, from: Data())
        
        //Assert
        XCTAssertNil(sut, "Movie should be created only with valid json data")
    }

    func testInit_withNoMovieIdInDictionary_shouldFailInitialization() {
        //Arrange
        let movieDictionary = ["title":"Star Wars"]
        let data = try! JSONSerialization.data(withJSONObject: movieDictionary, options: .prettyPrinted)
        let movie = try? JSONDecoder().decode(MovieImpl.self, from: data)
        
        // Assert
        XCTAssertNil(movie, "Movie should only be created if dictionary has movie Id")
    }

    func testInit_withMovieTitleInDictionary_shouldSetMovieTitle() {
        //Arrange
        let jsonMovieDictionary = ["id":1234, "title":"Star Wars"] as [String : Any]
        let data = try! JSONSerialization.data(withJSONObject: jsonMovieDictionary, options: .prettyPrinted)
        let movie = try? JSONDecoder().decode(MovieImpl.self, from: data)

        //Assert
        XCTAssertEqual(movie?.title, "Star Wars", "Movie should be created with title when available in the dictionary")
    }
    
    func testInit_withMovieTitleInDictionary_shouldSetMovieVoteAverage() {
        //Arrange
        let jsonMovieDictionary = ["id":1234, "vote_average":7.0] as [String : Any]
        let data = try! JSONSerialization.data(withJSONObject: jsonMovieDictionary, options: .prettyPrinted)
        let movie = try? JSONDecoder().decode(MovieImpl.self, from: data)
        
        //Assert
        XCTAssertEqual(movie?.voteAverage, 7.0, "Movie should be created with vote average when available in the dictionary")
    }
}
