//
//  MovieManagerTests.swift
//  MovieAppTests
//
//  Created by Waheed Malik on 01/01/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import XCTest
@testable import MovieApp

extension MovieManagerTests {
    class MockMovieAPIClient: MovieAPIClient {
        func fetchCollectionMovies(ofCollectionId collectionId: Int, completionHandler: @escaping (Result<[Movie]>) -> Void) {
        }
        
        func fetchDetail(ofMovie movie: Movie, completionHandler: @escaping (Result<Movie>) -> Void) {
        }
        
        func photo(withPosterPath posterPath: String, _ completionHandler: @escaping (Result<UIImage>) -> Void) {
        }
        
        var fetchNowShowingMoviesCalled = false
        var completionHandler: ((Result<[Movie]>) -> Void)?
        
        func fetchNowShowingMovies(completionHandler: @escaping (_ result: Result<[Movie]>) -> Void) {
            fetchNowShowingMoviesCalled = true
            self.completionHandler = completionHandler
        }
    }
    
    class MockImageStore: ImageStore {
        var getImageCalledWithImageUrl: String!
        var completionHandler: ((Result<UIImage>) -> Void)?
        func getImage(with imageUrl: String, _ completionHandler: @escaping (Result<UIImage>) -> Void) {
            getImageCalledWithImageUrl = imageUrl
            self.completionHandler = completionHandler
        }
    }
}

class MovieManagerTests: XCTestCase {
    
    var mockMovieAPIClient: MockMovieAPIClient!
    var sut: MovieManagerImpl!
    
    override func setUp() {
        super.setUp()
        mockMovieAPIClient = MockMovieAPIClient()
        sut = MovieManagerImpl(apiClient: mockMovieAPIClient)
    }
    
    func testConformanceToMovieManagerProtocol() {
        // Assert
        XCTAssertTrue((sut as AnyObject) is MovieManager, "MovieManagerImpl should conforms to MovieManager protocol")
    }
    
    func testLoadNowShowingMovies_asksMovieAPIClientToFetchNowShowingMovies() {
        // Act
        sut.loadNowShowingMovies { result in
            
        }
        
        // Assert
        XCTAssert(mockMovieAPIClient.fetchNowShowingMoviesCalled)
    }
    
    func testLoadNowShowingMovies_shouldReceiveMoviesWithNilError_whenAPIClientFoundMovies() {
        // Arrange
        var receivedMovies : [Movie]?
        var receivedError : Error?
        let expectedMovies = [MovieImpl(id: 1234)]
        
        // Act
        sut.loadNowShowingMovies { (result: Result<[Movie]>) in
            switch result {
            case .success(let movies):
                receivedMovies = movies
            case .failure(let error):
                receivedError = error
            }
        }
        
        mockMovieAPIClient.completionHandler?(.success(expectedMovies))
        
        // Assert
        XCTAssertNil(receivedError)
        XCTAssertNotNil(receivedMovies)
        XCTAssert(expectedMovies.first! == receivedMovies!.first!)
    }
    
    func testLoadNowShowingMovies_shouldReceiveNoNowShowingMoviesFoundError_whenAPIClientHasInvalidDataError() {
        // Arrange
        var receivedMovies : [Movie]?
        var receivedError : Error?
        
        // Act
        sut.loadNowShowingMovies { (result: Result<[Movie]>) in
            switch result {
            case .success(let movies):
                receivedMovies = movies
            case .failure(let error):
                receivedError = error
            }
        }
        mockMovieAPIClient.completionHandler?(.failure(MovieAPIClientError.invalidDataError))
        
        //Assert
        XCTAssertEqual(receivedError as! MovieManagerError, MovieManagerError.noNowShowingMoviesFoundError, "Should receive noNowShowingMoviesFoundError when underlying api has InvalidDataError")
        XCTAssertNil(receivedMovies, "No movies are returned when error occured")
    }
    
    func testLoadNowShowingMovies_shouldReceiveNoMoviesFoundError_whenAPIClientHasDataDecodingError() {
        // Arrange
        var receivedMovies : [Movie]?
        var receivedError : Error?
        
        // Act
        sut.loadNowShowingMovies { (result: Result<[Movie]>) in
            switch result {
            case .success(let movies):
                receivedMovies = movies
            case .failure(let error):
                receivedError = error
            }
        }
        mockMovieAPIClient.completionHandler?(.failure(MovieAPIClientError.dataDecodingError))
        
        //Assert
        XCTAssertEqual(receivedError as! MovieManagerError, MovieManagerError.noNowShowingMoviesFoundError, "Should receive noNowShowingMoviesFoundError when underlying api has DataSerializationError")
        XCTAssertNil(receivedMovies, "No movies are returned when error occured")
    }
    
    func testLoadNowShowingMovies_shouldReceiveConnectionError_whenAPIClientHasHttpError() {
        // Arrange
        var receivedMovies : [Movie]?
        var receivedError : Error?
        
        // Act
        sut.loadNowShowingMovies { (result: Result<[Movie]>) in
            switch result {
            case .success(let movies):
                receivedMovies = movies
            case .failure(let error):
                receivedError = error
            }
        }
        mockMovieAPIClient.completionHandler?(.failure(MovieAPIClientError.httpError))
        
        //Assert
        XCTAssertEqual(receivedError as! MovieManagerError, MovieManagerError.connectionError, "Should receive ConnectionError when underlying api has HttpError")
        XCTAssertNil(receivedMovies, "No movies are returned when error occured")
    }
}
