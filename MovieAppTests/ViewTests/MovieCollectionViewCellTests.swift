//
//  MovieCollectionViewCellTests.swift
//  MovieAppTests
//
//  Created by Waheed Malik on 02/01/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import XCTest
@testable import MovieApp

extension MovieCollectionViewCellTests {
    class FakeDataSource: NSObject, UICollectionViewDataSource {
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return 1
        }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            return MovieCollectionViewCell()
        }
    }
    
    class FakeMovieViewModel: MovieViewModel {
        var voting: String = ""
        var movie: Movie { return MovieImpl(id: 123) }
        var title: String { return "Star Wars"}
    }
}

class MovieCollectionViewCellTests: XCTestCase {
    let fakeDataSource = FakeDataSource()
    var collectionView : UICollectionView!
    var cell : MovieCollectionViewCell!
    
    override func setUp() {
        super.setUp()
        let storyBoard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
        let nowShowingMoviesVC = storyBoard.instantiateViewController(withIdentifier: "NowShowingMoviesVC") as! NowShowingMoviesVC
        UIApplication.shared.keyWindow?.rootViewController = nowShowingMoviesVC
        _ = nowShowingMoviesVC.view
        collectionView = nowShowingMoviesVC.moviesCollectionView
        collectionView.dataSource = fakeDataSource
        cell = (collectionView.dequeueReusableCell(withReuseIdentifier: "MovieCell", for: IndexPath(row: 0, section: 0)) as! MovieCollectionViewCell)
    }
    
    func testMovieCell_HasTitleLabel() {
        // Assert
        XCTAssertNotNil(cell.movieNameLabel)
    }

    func testMovieCell_SetTitleOnLabel() {
        //Act
        cell.configCell(with: FakeMovieViewModel())
        // Assert
        XCTAssertEqual(cell.movieNameLabel.text!, "Star Wars")
    }
}
