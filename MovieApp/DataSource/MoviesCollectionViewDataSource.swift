//
//  MoviesCollectionViewDataSource.swift
//  MovieApp
//
//  Created by Waheed Malik on 02/01/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import UIKit

class MoviesCollectionViewDataSource: NSObject, UICollectionViewDataSource {
    var viewModel: BaseViewModel?
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let viewModel = viewModel {
            return viewModel.numberOfMovies()
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MovieCell", for: indexPath)
        if let movieCell = cell as? MovieCollectionViewCell, let movieVM = viewModel?.movie(at: indexPath.item) {
            movieCell.configCell(with: movieVM)
            loadPhoto(with: movieVM.movie.posterPath, onCell: movieCell)
        }
        return cell
    }
    
    private func loadPhoto(with photoPath: String?, onCell cell:MovieCollectionViewCell) {
        guard let photoPath = photoPath else { return }
        ImageStore.getImage(withImagePath: photoPath) { (result: Result<UIImage>) in
            if case let Result.success(image) = result {
                cell.setPosterImage(image: image)
            }
        }
    }
}
