//
//  NowShowingMoviesCoordinator.swift
//  MovieApp
//
//  Created by Waheed Malik on 02/01/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import UIKit

class NowShowingMoviesCoordinator : Coordinator {
    // MARK:- PRIVATE VARIABLES
    private var navigationController: UINavigationController
    private var movieDetailCoordinator: MovieDetailCoordinator?
    
    // MARK:- INITIALIZER
    init(navController: UINavigationController) {
        self.navigationController = navController
    }
    
    // MARK:- PUBLIC METHODS
    func start() {
        if let nowShowingMoviesVC = navigationController.topViewController as? NowShowingMoviesVC {
            let movieManager = SharedComponentsDir.movieManager
            let moviesCollectionViewDataSource = MoviesCollectionViewDataSource()
            let moviesCollectionViewDelegate = MoviesCollectionViewDelegate()
            let nowShowingMoviesViewModel = NowShowingMoviesViewModelImpl(movieManager: movieManager, viewDelegate: nowShowingMoviesVC, coordinatorDelegate: self)
            
            nowShowingMoviesVC.viewModel = nowShowingMoviesViewModel
            nowShowingMoviesVC.moviesCollectionViewDataSource = moviesCollectionViewDataSource
            nowShowingMoviesVC.moviesCollectionViewDelegate = moviesCollectionViewDelegate
            moviesCollectionViewDelegate.viewModel = nowShowingMoviesViewModel
            moviesCollectionViewDataSource.viewModel = nowShowingMoviesViewModel
        }
    }
}

extension NowShowingMoviesCoordinator: NowShowingMoviesViewModelCoordinatorDelegate {
    func didSelect(movie: Movie) {
        movieDetailCoordinator = MovieDetailCoordinator(navController: self.navigationController, selectedMovie: movie)
        movieDetailCoordinator?.start()
    }
}
