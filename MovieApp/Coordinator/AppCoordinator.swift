//
//  AppCoordinator.swift
//  MovieApp
//
//  Created by Waheed Malik on 02/01/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import UIKit

class AppCoordinator : Coordinator {
    // MARK:- PRIVATE VARIABLES
    private var window: UIWindow
    private var nowShowingMoviesCoordinator : NowShowingMoviesCoordinator?
    
    // MARK:- INITIALIZER
    init(window: UIWindow) {
        self.window = window
    }
    
    // MARK:- PUBLIC METHODS
    func start() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let rootNavController = storyboard.instantiateInitialViewController() as? UINavigationController {
            self.window.rootViewController = rootNavController
            self.showNowShowingMovies(rootNavController: rootNavController)
        }
    }
    
    // MARK:- PRIVATE METHODS
    private func showNowShowingMovies(rootNavController: UINavigationController) {
        nowShowingMoviesCoordinator = NowShowingMoviesCoordinator(navController: rootNavController)
        nowShowingMoviesCoordinator?.start()
    }
}


