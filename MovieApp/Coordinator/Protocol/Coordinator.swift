//
//  Coordinator.swift
//  MovieApp
//
//  Created by Waheed Malik on 02/01/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import Foundation

protocol Coordinator {
    func start()
}
