//
//  MovieDetailCoordinator.swift
//  MovieApp
//
//  Created by Waheed Malik on 02/01/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import UIKit

class MovieDetailCoordinator : Coordinator {
    // MARK:- PRIVATE VARIABLES
    private var navigationController: UINavigationController
    private var movieDetailCoordinator: MovieDetailCoordinator?
    private var selectedMovie: Movie
    
    // MARK:- INITIALIZER
    init(navController: UINavigationController, selectedMovie: Movie) {
        self.navigationController = navController
        self.selectedMovie = selectedMovie
    }
    
    // MARK:- PUBLIC METHODS
    func start() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let movieDetailVC = storyboard.instantiateViewController(withIdentifier: "MovieDetailVC") as? MovieDetailVC {
            let movieManager = SharedComponentsDir.movieManager
            let moviesCollectionViewDataSource = MoviesCollectionViewDataSource()
            let moviesCollectionViewDelegate = MoviesCollectionViewDelegate()
            let movieDetailViewModel = MovieDetailViewModelImpl(selectedMovie: self.selectedMovie, movieManager: movieManager, viewDelegate: movieDetailVC, coordinatorDelegate: self)
            
            movieDetailVC.viewModel = movieDetailViewModel
            movieDetailVC.moviesCollectionViewDataSource = moviesCollectionViewDataSource
            movieDetailVC.moviesCollectionViewDelegate = moviesCollectionViewDelegate
            moviesCollectionViewDelegate.viewModel = movieDetailViewModel
            moviesCollectionViewDataSource.viewModel = movieDetailViewModel
            self.navigationController.pushViewController(movieDetailVC, animated: true)
        }
    }
}

extension MovieDetailCoordinator: MovieDetailViewModelCoordinatorDelegate {
    func didSelect(movie: Movie) {
        movieDetailCoordinator = MovieDetailCoordinator(navController: self.navigationController, selectedMovie: movie)
        movieDetailCoordinator?.start()
    }
}
