//
//  MovieCollectionViewCell.swift
//  MovieApp
//
//  Created by Waheed Malik on 02/01/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import UIKit

class MovieCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var movieNameLabel: UILabel!
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var votingLabel: UILabel!
    
    func configCell(with movieViewModel: MovieViewModel) {
        movieNameLabel.text = movieViewModel.title
        votingLabel.text = movieViewModel.voting
    }
    
    func setPosterImage(image: UIImage) {
        self.posterImageView.image = image
    }
    
    override func prepareForReuse() {
        self.posterImageView.image = UIImage(named: "poster-placeholder")
    }
}
