//
//  ImageStore.swift
//  MovieApp
//
//  Created by Waheed Malik on 01/01/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import UIKit

// This class retrived the photo if it is already cached otherwise downloads and then caches the image.
class ImageStore {
    private static var imageCache = NSCache<NSString, UIImage>()
    static func getImage(withImagePath imagePath: String, _ completionHandler: @escaping (Result<UIImage>) -> Void) {
        let nsImagePath = imagePath as NSString
        if let image = imageCache.object(forKey: nsImagePath) {
            completionHandler(.success(image))
        } else {
            SharedComponentsDir.movieDbAPIClient.photo(withPosterPath: imagePath, { (result: Result<UIImage>) in
                switch result {
                case .success(let image):
                    imageCache.setObject(image, forKey: nsImagePath)
                case .failure(let error):
                    print("Unable to download image. Error: \(error)")
                }
                completionHandler(result)
            })
        }
    }
}

