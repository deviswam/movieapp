//
//  MovieManager.swift
//  MovieApp
//
//  Created by Waheed Malik on 01/01/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import Foundation

enum MovieManagerError : Error {
    case noNowShowingMoviesFoundError
    case noMovieDetailFoundError
    case noRelatedMoviesFoundError
    case connectionError
    
    func userFriendlyMessage() -> String {
        switch self {
        case .noNowShowingMoviesFoundError:
            return "No Now Showing Movies Found"
        case .noMovieDetailFoundError:
            return "No Movie Detail Found"
        case .noRelatedMoviesFoundError:
            return "No Related Movies Found"
        case .connectionError:
            return "Connection Error"
        }
    }
}

//MARK: INTERNAL ENUM
enum MovieManagerMethodType {
    case nowShowingMovies
    case movieDetail
    case relatedMovies
}

protocol MovieManager {
    // get now showing movies
    func loadNowShowingMovies(completionHandler: @escaping (_ result: Result<[Movie]>) -> Void)
    // gets details of a movie
    func loadDetail(ofMovie movie: Movie, completionHandler: @escaping (_ result: Result<Movie>) -> Void)
    // get related movies
    func loadRelatedMovies(ofMovie movie: Movie, completionHandler: @escaping (Result<[Movie]>) -> Void)
}

class MovieManagerImpl: MovieManager {
    
    // MARK: PRIVATE VARIABLES
    private let apiClient: MovieAPIClient
    
    // MARK: INITIALIZER
    init(apiClient: MovieAPIClient) {
        self.apiClient = apiClient
    }
    
    // MARK: PUBLIC METHODS
    func loadNowShowingMovies(completionHandler: @escaping (_ result: Result<[Movie]>) -> Void) {
        apiClient.fetchNowShowingMovies { [unowned self] (result: Result<[Movie]>) in
            var mResult = result
            if case let Result.failure(error) = result {
                mResult = .failure(self.movieManagerError(from: error as! MovieAPIClientError, for: .nowShowingMovies))
            }
            completionHandler(mResult)
        }
    }
    
    func loadDetail(ofMovie movie: Movie, completionHandler: @escaping (_ result: Result<Movie>) -> Void) {
        apiClient.fetchDetail(ofMovie: movie) {  [unowned self] (result: Result<Movie>) in
            var mResult = result
            if case let Result.failure(error) = result {
                mResult = .failure(self.movieManagerError(from: error as! MovieAPIClientError, for: .movieDetail))
            }
            completionHandler(mResult)
        }
    }
    
    func loadRelatedMovies(ofMovie movie: Movie, completionHandler: @escaping (Result<[Movie]>) -> Void) {
        guard let collectionId = movie.collectionId else {
            completionHandler(.failure(MovieManagerError.noRelatedMoviesFoundError))
            return
        }
        
        apiClient.fetchCollectionMovies(ofCollectionId: collectionId) { [unowned self] (result: Result<[Movie]>)in
            var mResult: Result<[Movie]>!
            switch result {
            case .success(let movies):
                let filteredMovies = movies.filter {
                    $0.id != movie.id
                }
                mResult = .success(filteredMovies)
            case .failure(let error):
                mResult = .failure(self.movieManagerError(from: error as! MovieAPIClientError, for: .relatedMovies))
            }
            completionHandler(mResult)
        }
    }
    
    // MARK: PRIVATE METHODS
    private func movieManagerError(from apiError: MovieAPIClientError, for methodType: MovieManagerMethodType) -> MovieManagerError {
        switch apiError {
        case .dataDecodingError, .invalidDataError:
            switch methodType {
            case .nowShowingMovies:
                return MovieManagerError.noNowShowingMoviesFoundError
            case .relatedMovies:
                return MovieManagerError.noRelatedMoviesFoundError
            case .movieDetail:
                return MovieManagerError.noMovieDetailFoundError
            }
        case .httpError:
            return MovieManagerError.connectionError
        }
    }
}
