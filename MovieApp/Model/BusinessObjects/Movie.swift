//
//  Movie.swift
//  MovieApp
//
//  Created by Waheed Malik on 01/01/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import Foundation

protocol Movie: Decodable {
    var id : Int { get }
    var title: String? { get }
    var voteAverage: Float? { get }
    var posterPath: String? { get }
    
    var releaseDate: Date? { get set}
    var overView: String? { get set }
    var collectionId: Int? { get set }
}

struct MovieImpl: Movie {
    var id: Int
    var title: String?
    var voteAverage: Float?
    var posterPath: String?
    
    var releaseDate: Date?
    var overView: String?
    var collectionId: Int?
    
    init(id: Int, title: String? = nil, voteAverage: Float? = nil, posterPath: String? = nil) {
        self.id = id
        self.title = title
        self.voteAverage = voteAverage
        self.posterPath = posterPath
    }
    
    enum CodingKeys: String, CodingKey {
        case id
        case title
        case voteAverage = "vote_average"
        case posterPath = "poster_path"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        let _id = try values.decode(Int.self, forKey: .id)
        let _title = try values.decodeIfPresent(String.self, forKey: .title)
        let _voteAverage = try values.decodeIfPresent(Float.self, forKey: .voteAverage)
        let _posterPath = try values.decodeIfPresent(String.self, forKey: .posterPath)
        
        self.init(id: _id, title: _title, voteAverage: _voteAverage, posterPath: _posterPath)
    }
}

func == (lhs: Movie, rhs: Movie) -> Bool {
    if lhs.id == rhs.id {
        return true
    }
    return false
}
