//
//  MovieAPIClient.swift
//  MovieApp
//
//  Created by Waheed Malik on 01/01/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import UIKit

enum MovieAPIClientError: Error {
    case httpError
    case invalidDataError
    case dataDecodingError
}

enum MovieListRootNode: String {
    case results = "results"
    case parts = "parts"
}

protocol MovieAPIClient {
    func fetchNowShowingMovies(completionHandler: @escaping (_ result: Result<[Movie]>) -> Void)
    func fetchDetail(ofMovie movie: Movie, completionHandler: @escaping (_ result: Result<Movie>) -> Void)
    func fetchCollectionMovies(ofCollectionId collectionId: Int, completionHandler: @escaping (_ result: Result<[Movie]>) -> Void)
    func photo(withPosterPath posterPath: String, _ completionHandler: @escaping (Result<UIImage>) -> Void)
}

class MovieDbAPIClient: MovieAPIClient {
    // MARK: PRIVATE VARIABLES
    private let BASE_URL = "https://api.themoviedb.org/3"
    private let BASE_PHOTO_URL = "https://image.tmdb.org/t/p/w185"
    private let commonRequestParams = ["api_key": "46f5de988505949c5cd45158ce309633", "language": "en-GB"]
    private let urlSession: URLSession!
    
    // MARK: PRIVATE DATA STRUCTURE
    private struct MoviesFeed: Decodable {
        let moviesList: [MovieImpl]
        private static var movieListRootNode: MovieListRootNode?
        
        struct MoviesFeedKey : CodingKey {
            var stringValue: String
            init?(stringValue: String) {
                self.stringValue = stringValue
            }
            var intValue: Int? { return nil }
            init?(intValue: Int) { return nil }
            
            static func makeKey(name: String) -> MoviesFeedKey {
                return MoviesFeedKey(stringValue: name)!
            }
        }
        
        init(from coder: Decoder) throws {
            guard let rootNode = MoviesFeed.movieListRootNode else {
                throw MovieAPIClientError.invalidDataError
            }
            
            let container = try coder.container(keyedBy: MoviesFeedKey.self)
            self.moviesList = try container.decode([MovieImpl].self, forKey: .makeKey(name:rootNode.rawValue))
        }
        
        // MARK: FACTORY METHOD
        static func getMoviesFeed(forData data:Data, withRootNode rootNode: MovieListRootNode) throws -> MoviesFeed {
            let decoder = JSONDecoder()
            movieListRootNode = rootNode
            let moviesFeed = try decoder.decode(MoviesFeed.self, from: data)
            return moviesFeed
        }
    }
    
    // MARK: INITIALIZER
    init(urlSession: URLSession = URLSession.shared) {
        self.urlSession = urlSession
    }
    
    // MARK: PUBLIC METHODS
    func photo(withPosterPath posterPath: String, _ completionHandler: @escaping (Result<UIImage>) -> Void) {
        guard let url = URL(string: BASE_PHOTO_URL + posterPath) else {
            return
        }
        //print("WAM PHOTO URL:\(url.absoluteString)")
        let request = URLRequest(url: url, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 10)
        let dataTask = urlSession.dataTask(with: request) { (data, urlResponse, error) in
            var result: Result<UIImage>!
            if error != nil {
                result = .failure(MovieAPIClientError.httpError)
            } else if let data = data {
                let image = UIImage(data: data)
                if image == nil {
                    result = .failure(MovieAPIClientError.invalidDataError)
                } else {
                    result = .success(image!)
                }
            }
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
        dataTask.resume()
    }
    
    func fetchNowShowingMovies(completionHandler: @escaping (_ result: Result<[Movie]>) -> Void) {
        let requestParams = ["region": "GB"]
        guard let urlRequest = self.createURLRequest(withParams: requestParams, forMethod: "/movie/now_playing") else {
            return
        }
        self.getMovies(with: urlRequest, shouldFetchNowShowingMovies: true) { (result: Result<[Movie]>) in
            completionHandler(result)
        }
    }
    
    func fetchDetail(ofMovie movie: Movie, completionHandler: @escaping (_ result: Result<Movie>) -> Void) {
        guard let urlRequest = self.createURLRequest(forMethod: "/movie/\(movie.id)") else {
            return
        }
        self.getDetail(ofMovie: movie, withRequest: urlRequest) { (result: Result<Movie>) in
            completionHandler(result)
        }
    }
    
    func fetchCollectionMovies(ofCollectionId collectionId: Int, completionHandler: @escaping (_ result: Result<[Movie]>) -> Void) {
        guard let urlRequest = self.createURLRequest(forMethod: "/collection/\(collectionId)") else {
            return
        }
        self.getMovies(with: urlRequest, shouldFetchNowShowingMovies: false) { (result: Result<[Movie]>) in
            completionHandler(result)
        }
    }
    
    // MARK: PRIVATE METHODS
    private func getDetail(ofMovie movie: Movie, withRequest urlRequest: URLRequest, completionHandler: @escaping (_ result: Result<Movie>) -> Void) {
        var movie = movie
        let dataTask = urlSession.dataTask(with: urlRequest) { (data, urlResponse, error) in
            var result: Result<Movie>!
            
            if error != nil {
                result = .failure(MovieAPIClientError.httpError)
            } else if let data = data {
                do {
                    if let dictionary = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any] {
                        if let overview = dictionary["overview"] as? String {
                            movie.overView = overview
                        }
                        
                        if let releaseDateStr = dictionary["release_date"] as? String,
                            let releaseDate = releaseDateStr.date() {
                            movie.releaseDate = releaseDate
                        }
                        
                        if let belongsToCollectionDic = dictionary["belongs_to_collection"] as? [String: AnyObject],
                            let collectionId = belongsToCollectionDic["id"] as? Int {
                            movie.collectionId = collectionId
                        }
                        result = .success(movie)
                    }
                } catch {
                    result = .failure(MovieAPIClientError.dataDecodingError)
                }
            }
            
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
        dataTask.resume()
    }
    
    private func getMovies(with urlRequest: URLRequest, shouldFetchNowShowingMovies: Bool, completionHandler: @escaping (_ result: Result<[Movie]>) -> Void) {
        
        let rootNodeName = shouldFetchNowShowingMovies ? MovieListRootNode.results :  MovieListRootNode.parts
        
        let dataTask = urlSession.dataTask(with: urlRequest) { (data, urlResponse, error) in
            var result: Result<[Movie]>!
            
            if error != nil {
                result = .failure(MovieAPIClientError.httpError)
            } else if let data = data {
                do {
                    let moviesFeed = try MoviesFeed.getMoviesFeed(forData: data, withRootNode: rootNodeName)
                    result = .success(moviesFeed.moviesList)
                } catch {
                    result = .failure(MovieAPIClientError.dataDecodingError)
                }
            }
            
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
        dataTask.resume()
    }
    
    private func createURLRequest(withParams parameters:[String: String] = [:], forMethod method: String) -> URLRequest? {
        // create url
        var urlComponents = URLComponents(string: BASE_URL + method)
        
        // append custom parameters
        var queryItems = [URLQueryItem]()
        for (key, value) in parameters {
            let query = URLQueryItem(name: key, value: value)
            queryItems.append(query)
        }
        // append common params
        commonRequestParams.forEach { queryItems.append(URLQueryItem(name: $0, value: $1))}
        
        urlComponents?.queryItems = queryItems
        guard let url = urlComponents?.url else { return nil }
        print("WAM URL:\(url.absoluteString)")
        
        // create request object
        let request = URLRequest(url: url, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 15)
        
        return request
    }
}
