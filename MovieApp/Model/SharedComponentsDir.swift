//
//  SharedComponentsDir.swift
//  MovieApp
//
//  Created by Waheed Malik on 02/01/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import Foundation

class SharedComponentsDir {
    static let movieManager: MovieManager = {
        let movieManager = MovieManagerImpl(apiClient: SharedComponentsDir.movieDbAPIClient)
        return movieManager
    }()
    
    static let movieDbAPIClient: MovieAPIClient = {
        let movieDbAPIClient = MovieDbAPIClient()
        return movieDbAPIClient
    }()
}
