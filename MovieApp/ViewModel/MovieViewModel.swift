//
//  MovieViewModel.swift
//  MovieApp
//
//  Created by Waheed Malik on 01/01/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import Foundation

protocol MovieViewModel {
    var title: String { get }
    var voting: String { get }
    
    var movie: Movie { get }
}

class MovieViewModelImpl: MovieViewModel {
    var title: String = ""
    var voting: String = ""
    var movie: Movie
    
    init(movie: Movie) {
        self.movie = movie
        if let title = movie.title {
            self.title = title
        }
        
        if let voting = movie.voteAverage {
            self.voting = "\(voting)"
        }
    }
}
