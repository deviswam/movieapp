//
//  NowShowingMoviesViewModel.swift
//  MovieApp
//
//  Created by Waheed Malik on 01/01/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import Foundation

// MARK:- COORDINATOR DELEGATE PROTOCOL
protocol NowShowingMoviesViewModelCoordinatorDelegate: class {
    func didSelect(movie: Movie)
}

// MARK:- VIEW DELEGATE PROTOCOL
protocol NowShowingMoviesViewModelViewDelegate: class {
    func nowShowingMoviesLoaded(withResult result: Result<Void>)
}

// MARK:- VIEWMODEL PROTOCOL
protocol NowShowingMoviesViewModel {
    func getNowShowingMovies()
    func selectedMovie(at index: Int)
}

class NowShowingMoviesViewModelImpl: BaseViewModel, NowShowingMoviesViewModel {
    // MARK: PRIVATE VARIABLES
    private let movieManager: MovieManager
    private weak var viewDelegate: NowShowingMoviesViewModelViewDelegate!
    private weak var coordinatorDelegate: NowShowingMoviesViewModelCoordinatorDelegate!
    
    // MARK: INITIALIZER
    init(movieManager: MovieManager, viewDelegate: NowShowingMoviesViewModelViewDelegate, coordinatorDelegate: NowShowingMoviesViewModelCoordinatorDelegate) {
        self.movieManager = movieManager
        self.viewDelegate = viewDelegate
        self.coordinatorDelegate = coordinatorDelegate
    }
    
    // MARK: PUBLIC METHODS
    func getNowShowingMovies() {
        var vmResult: Result<Void>!
        movieManager.loadNowShowingMovies { [weak self] result in
            switch result {
            case .success(let movies):
                self?.movies = movies
                vmResult = .success(())
            case .failure(let error):
                vmResult = .failure(error)
            }
            self?.viewDelegate.nowShowingMoviesLoaded(withResult: vmResult)
        }
    }
    
    override func selectedMovie(at index: Int) {
        guard let movies = self.movies, index < movies.count else {
            return
        }
        self.coordinatorDelegate.didSelect(movie: movies[index])
    }
}
