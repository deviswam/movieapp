//
//  MovieDetailViewModel.swift
//  MovieApp
//
//  Created by Waheed Malik on 02/01/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import UIKit

// MARK:- COORDINATOR DELEGATE PROTOCOL
protocol MovieDetailViewModelCoordinatorDelegate: class {
    func didSelect(movie: Movie)
}

// MARK:- VIEW DELEGATE PROTOCOL
protocol MovieDetailViewModelViewDelegate: class {
    func movieDetailLoaded(with result: Result<Void>)
    func relatedMoviesLoaded(with result: Result<Void>)
}

// MARK:- VIEWMODEL PROTOCOL
protocol MovieDetailViewModel {
    var title: String { get }
    var overView: String { get }
    var releaseDate: String { get }
    var voting: String { get }
    
    func getDetailsOfSelectedMovie()
    func posterImage(completion: @escaping (UIImage) -> Void)
}

class MovieDetailViewModelImpl: BaseViewModel, MovieDetailViewModel {
    // MARK: PRIVATE VARIABLES
    private var selectedMovie: Movie
    private let movieManager: MovieManager
    private weak var viewDelegate: MovieDetailViewModelViewDelegate!
    private weak var coordinatorDelegate: MovieDetailViewModelCoordinatorDelegate!
    
    // MARK: INITIALIZER
    init(selectedMovie: Movie, movieManager: MovieManager, viewDelegate: MovieDetailViewModelViewDelegate, coordinatorDelegate: MovieDetailViewModelCoordinatorDelegate) {
        self.selectedMovie = selectedMovie
        self.movieManager = movieManager
        self.viewDelegate = viewDelegate
        self.coordinatorDelegate = coordinatorDelegate
    }
    
    func getDetailsOfSelectedMovie() {
        var vmResult: Result<Void>!
        movieManager.loadDetail(ofMovie: selectedMovie) { [weak self] result in
            switch result {
            case .success(let movie):
                self?.selectedMovie = movie
                vmResult = .success(())
                self?.getRelatedMovies()
            case .failure(let error):
                vmResult = .failure(error)
            }
            self?.viewDelegate.movieDetailLoaded(with: vmResult)
        }
    }
    
    var title: String {
        return selectedMovie.title ?? ""
    }
    
    var overView: String {
        return selectedMovie.overView ?? ""
    }
    
    var releaseDate: String {
        guard let relDate = selectedMovie.releaseDate?.formattedDate() else {
            return ""
        }
        return "Release date: \(relDate)"
    }
    
    var voting: String {
        guard let voting = selectedMovie.voteAverage else {
            return ""
        }
        return "Vote avg: \(voting)"
    }
    
    func posterImage(completion: @escaping (UIImage) -> Void) {
        if let posterPath = self.selectedMovie.posterPath {
            ImageStore.getImage(withImagePath: posterPath) { (result: Result<UIImage>) in
                if case let Result.success(image) = result {
                    completion(image)
                }
            }
        }
    }
    
    private func getRelatedMovies() {
        var vmResult: Result<Void>!
        movieManager.loadRelatedMovies(ofMovie: selectedMovie) { [weak self] result in
            switch result {
            case .success(let movies):
                self?.movies = movies
                vmResult = .success(())
            case .failure(let error):
                vmResult = .failure(error)
            }
            self?.viewDelegate.relatedMoviesLoaded(with: vmResult)
        }
    }
    
    override func selectedMovie(at index: Int) {
        guard let movies = self.movies, index < movies.count else {
            return
        }
        self.coordinatorDelegate.didSelect(movie: movies[index])
    }
}

