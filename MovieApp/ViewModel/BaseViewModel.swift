//
//  BaseMoviesViewModel.swift
//  MovieApp
//
//  Created by Waheed Malik on 03/01/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import Foundation

class BaseViewModel {
    var movies: [Movie]?
    
    func numberOfMovies() -> Int {
        guard let movies = self.movies else {
            return 0
        }
        return movies.count
    }
    
    func movie(at index: Int) -> MovieViewModel? {
        if let movies = self.movies, index < movies.count {
            return MovieViewModelImpl(movie: movies[index])
        }
        return nil
    }
    
    func selectedMovie(at index: Int) { }
}
