//
//  NowShowingMoviesVC.swift
//  MovieApp
//
//  Created by Waheed Malik on 02/01/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import UIKit

class NowShowingMoviesVC: BaseVC {
    var viewModel: NowShowingMoviesViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // get movies list
        viewModel?.getNowShowingMovies()
    }
}

extension NowShowingMoviesVC: NowShowingMoviesViewModelViewDelegate {
    func nowShowingMoviesLoaded(withResult result: Result<Void>) {
        switch result {
        case .success:
            self.moviesCollectionView.reloadData()
        case .failure:
            print("Info (Inform user): Unable to fetch now showing movies list")
        }
    }
}
