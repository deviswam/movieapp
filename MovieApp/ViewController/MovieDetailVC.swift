//
//  MovieDetailVC.swift
//  MovieApp
//
//  Created by Waheed Malik on 02/01/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import UIKit

class MovieDetailVC: BaseVC {
    
    @IBOutlet weak var movieNameLabel: UILabel!
    @IBOutlet weak var votingLabel: UILabel!
    @IBOutlet weak var releaseDateLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var relatedMoviesLabel: UILabel!
    
    var viewModel: MovieDetailViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel?.getDetailsOfSelectedMovie()
    }
}

extension MovieDetailVC: MovieDetailViewModelViewDelegate {
    func relatedMoviesLoaded(with result: Result<Void>) {
        switch result {
        case .success:
            self.moviesCollectionView.reloadData()
            self.relatedMoviesLabel.isHidden = false
        case .failure:
            print("Info (Inform user): Unable to fetch related movies")
        }
    }
    
    func movieDetailLoaded(with result: Result<Void>) {
        switch result {
        case .success:
            self.setControlValuesWithMovieDetail()
        case .failure:
            print("Info (Inform user): Unable to fetch movie detail")
        }
    }

    private func setControlValuesWithMovieDetail() {
        movieNameLabel.text = viewModel?.title
        votingLabel.text = viewModel?.voting
        releaseDateLabel.text = viewModel?.releaseDate
        overviewLabel.text = viewModel?.overView
        
        viewModel?.posterImage(completion: { [weak self] (movieImage) in
            self?.posterImageView.image = movieImage
        })
    }
}
