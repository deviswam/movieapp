//
//  BaseViewController.swift
//  MovieApp
//
//  Created by Waheed Malik on 03/01/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import UIKit

class BaseVC: UIViewController {
    @IBOutlet weak var moviesCollectionView: UICollectionView!
    var moviesCollectionViewDataSource: UICollectionViewDataSource?
    var moviesCollectionViewDelegate: UICollectionViewDelegateFlowLayout?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        moviesCollectionView.dataSource = moviesCollectionViewDataSource
        moviesCollectionView.delegate = moviesCollectionViewDelegate
        registerMovieCellWithCollectionView()
    }
    
    //MARK: PRIVATE METHODS
    private func registerMovieCellWithCollectionView() {
        let nib = UINib(nibName: "MovieCollectionViewCell", bundle: Bundle.main)
        moviesCollectionView.register(nib, forCellWithReuseIdentifier: "MovieCell")
    }
}
