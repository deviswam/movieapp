//
//  AppDelegate.swift
//  MovieApp
//
//  Created by Waheed Malik on 31/12/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var appCoordinator: AppCoordinator!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        // Return if this is a unit test
        if let _ = NSClassFromString("XCTest") {
            return true
        }
        
        window = UIWindow()
        appCoordinator = AppCoordinator(window: window!)
        appCoordinator.start()
        window?.makeKeyAndVisible()
        return true
    }
}

