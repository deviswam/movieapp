IMPORTANT NOTES
===============

1. The project fully implements the requested user stories with the acceptance criteria.
2. iOS 9 onwards supported. Tested on iPhone 6/7/8/10 their plus/Max/S sizes in both orientation.
3. The project has been developed on latest XCode 10.0 without any warnings or errors.
4. The app is following MVVM-C (Model-View-ViewModel-Coordinator) architecture pattern.
5. Test driven development approach has been adopted by writting test first with XCTest. Only suitable test cases have been written considering the time limitation and effort involved. Project Test suite has 44 test cases covering most of the core application components.
6. I preferred to use all the built-in native iOS technologies and not using third-party libraries or CocoaPods for this small project.
7. With more time, following things could be implemented/improved:
    * The clicking through navigation between movie detail and its related movies simply pushes the next view controller on top of the navigation stack, which is not ideal incase the related movies keep going. This can be optimised based on needs.
    * Aiming to achieve 100% test coverage by writing test cases for movie detail screen and its related MVVM-C module components. Most of those test cases would be same as now showing movies screen's MVVM-C module, whose test cases are already written.
    * Error handling unit tests as i was aiming to complete the happy path first. Despite of that application error handling code is stable and app doesn't crash on httpError, serialization scenarios or invalid data for example.
    * Test cases are not refactored to follow DRY principle hence there would be some repetition in few test cases.
    * Loading more records when user scrolls at the end of the list by using pagination of the MovieDB api.
    * User info messages are currently being shown in Xcode console incase of any issue with retriving data, this could be improved by showing alert to the user.
8. This project is built on best software engineering practices and easy to follow. To name a few: SOLID principles, design patterns, protocol oriented programming, loosely coupled architecture and TDD.

Thanks for your time.

